#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#define PIPE "/tmp/ppstats"

void read_and_print_pipe() {
    int stats[2];
    int fd = open(PIPE, O_RDONLY);
    int err = read(fd, stats, sizeof(stats));
    close(fd);

    printf("Got answer!\n");
    printf("%s%d%s%d%s", "Balls to right: ", stats[0], ", Balls to left: ", stats[1], "\n");
}

int main(int argc, char *argv[]) {
    if(argc < 1) exit(666);
    int pid = atoi(argv[1]);

    printf("%s%d%s", "PID: ", pid, "\n");
    do {
        int err = kill(pid, SIGRTMIN);
        if(err != 0) {
            printf("Process with such pid does not exist!\n");
            exit(0);
        }
        sleep(1);
    } while (access(PIPE, F_OK) == -1);

    read_and_print_pipe();

    return 0;
}