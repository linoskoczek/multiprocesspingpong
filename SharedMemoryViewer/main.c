#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/shm.h>

#define SHMKEY 0x654321

int main() {
    int sms = shmget(SHMKEY, sizeof(long), 0);
    int *p = shmat(sms, NULL, 0);
    for (int i = 0; i < 5; i++) {
        printf("%d%s", p[i], "\n");
    }
}