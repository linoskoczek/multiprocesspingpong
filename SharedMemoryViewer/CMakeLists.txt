cmake_minimum_required(VERSION 3.13)
project(SharedMemoryViewer C)

set(CMAKE_C_STANDARD 11)

add_executable(SharedMemoryViewer main.c)