//
// Created by marcin on 5/14/19.
//

#ifndef ZSO2_FUNCTIONS_H
#define ZSO2_FUNCTIONS_H

#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

int get_pid_of_next_process();

static void ball_received_signal_handler(int signal_number);

static void statistics_signal_handler(int signal_number);

void *statistics_manager();

void *throw_balls();

void add_signal_handlers();

void balls_action();

void child_process_start_actions();

void create_semaphore();

void create_shared_memory();

void create_subprocesses(int number);

bool check_rounds(int *data, int i);

void first_process_finish_actions();

void first_process_start_actions();

void print(const char *format, ...);

void wake_up_process_left();

void wake_up_process_right();

void write_statistics_via_named_pipe();

void check_if_should_exit(const int *data);

void semopen(struct sembuf semaphore_struct);

void semclose(struct sembuf semaphore_struct);

void print_stats(const int *data);

#endif //ZSO2_FUNCTIONS_H
