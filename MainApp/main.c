#include <wait.h>
#include "functions.h"

#define SHMKEY 0x654321
#define SEMKEY 0x123456
#define BALLS_NUMBER 3
#define SUBPROCESSES 10
#define ROUNDS_NUMBER 2

/*
 * Memory documentation:
 * (x) <- id of ball
 * [(1) process, (2) process, (3) process, (1) round, (2) round, (3) round, (1) direction, (2) direction, (3) direction]
 */

int sem;
struct sembuf sop;
union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
} arg;

int process_no = 0, first_process = 0, parent_process = 0, child_process = 0;
int balls_to_right = 0, balls_to_left = 0;
bool first = true, last = false, finished = false;
pthread_t throwing_thread, statistics_thread;
int shmid;
int *mem;

int main() {
    create_subprocesses(SUBPROCESSES);
    add_signal_handlers();
    srand(time(NULL));

    if (first) first_process_start_actions();
    else child_process_start_actions();

    while (!finished) pause();
    return 0;
}

void first_process_start_actions() {
    first_process = getpid();
    create_semaphore();
    create_shared_memory();
    sleep(1);
    pthread_create(&throwing_thread, NULL, throw_balls, NULL);
}

void first_process_finish_actions() {
    pthread_cancel(statistics_thread);
    if (child_process != 0) {
        kill(child_process, SIGRTMAX - 1);
        waitpid(child_process, NULL, 0);
    }
    semctl(sem, 0, IPC_RMID);
    shmctl(shmid, IPC_RMID, 0);
    unlink("/tmp/ppstats");
}

void child_process_start_actions() {
    sleep(1);
    shmid = shmget(SHMKEY, sizeof(int) * BALLS_NUMBER, 0);
    sem = semget(SEMKEY, 1, 0666);
}

void create_semaphore() {
    sem = semget(SEMKEY, 1, IPC_CREAT | 0660);
    arg.val = 1;
    semctl(sem, 0, SETVAL, arg);
}

void create_shared_memory() {
    shmid = shmget(SHMKEY, sizeof(int) * BALLS_NUMBER * 3, IPC_CREAT | 0660);
    mem = shmat(shmid, NULL, 0);
    int i = 0;
    for (i = 0; i < BALLS_NUMBER; i++) mem[i] = -1;
    for (; i < BALLS_NUMBER * 2; i++) mem[i] = ROUNDS_NUMBER;
    for (; i < BALLS_NUMBER * 3; i++) mem[i] = 1;
}

void balls_action() {
    semopen(sop);
    mem = shmat(shmid, 0, 0);
    for (int i = 0; i < BALLS_NUMBER; i++) {
        if (mem[i] == process_no) {
            if (first || last) {
                mem[i + BALLS_NUMBER * 2] *= -1;
                if (first) {
                    if (check_rounds(mem, i)) break;
                }
            }
            if (mem[i + BALLS_NUMBER * 2] > 0) {
                balls_to_right++;
                wake_up_process_right();
            } else {
                balls_to_left++;
                wake_up_process_left();
            }
            mem[i] += mem[i + BALLS_NUMBER * 2];
            break;
        }
    }
    usleep(rand() % 100000 + 100000);
    print_stats(mem);
    semclose(sop);
}

bool check_rounds(int *data, int i) {
    data[i + BALLS_NUMBER]--;
    if (data[i + BALLS_NUMBER] <= 0) {
        print("Ball %d out of the game!", i);
        data[i] = -1;
        check_if_should_exit(data);
        return true;
    }
    return false;
}

void check_if_should_exit(const int *data) {
    bool all_finished = true;
    for (int i = 0; i < BALLS_NUMBER; i++) {
        if (data[i] != -1) {
            all_finished = false;
            break;
        }
    }
    if (all_finished) {
        finished = true;
        first_process_finish_actions();
    }
}

void *throw_balls() {
    int balls_left = BALLS_NUMBER;
    while (balls_left > 0) {
        usleep(rand() % 1000000 + 1000000);

        semopen(sop);

        mem = shmat(shmid, 0, 0);
        for (int i = 0; i < BALLS_NUMBER; i++) {
            if (mem[i] == -1) {
                mem[i] = 1;
                balls_left--;
                break;
            }
        }
        print("New ball has came to the game!");
        wake_up_process_right();

        semclose(sop);
    }
    return 0;
}

void create_subprocesses(int number) {
    int result = 0;
    do {
        result = fork();
        if (result == 0) {
            process_no++;
            first = false;
        }
        number--;
    } while (result == 0 && number > 1);
    if (process_no == SUBPROCESSES - 1) last = true;
    parent_process = getppid();
    child_process = result;
    print("%s%d%s%d%s%d", "Hello, I'm process ", process_no, " Previous process: ", parent_process, " Next process: ",
          child_process);
}

void wake_up_process_right() {
    int pid = get_pid_of_next_process();
    print("Signal for %d", pid);
    kill(pid, SIGRTMAX);
}

void wake_up_process_left() {
    int pid = parent_process;
    print("Signal for %d", pid);
    kill(pid, SIGRTMAX);
}

int get_pid_of_next_process() {
    if (last) return parent_process;
    else return child_process;
}

void write_statistics_via_named_pipe() {
    print("Created pipe");
    mkfifo("/tmp/ppstats", 0666);
    int fd = open("/tmp/ppstats", O_WRONLY);
    int stats[2] = {balls_to_right, balls_to_left};
    write(fd, stats, sizeof(stats));
    close(fd);
    unlink("/tmp/ppstats");
}

void add_signal_handlers() {
    struct sigaction signal_action;
    signal_action.sa_handler = ball_received_signal_handler;
    signal_action.sa_flags = 0;
    sigaction(SIGRTMAX, &signal_action, NULL);
    sigaction(SIGRTMAX - 1, &signal_action, NULL);

    pthread_create(&statistics_thread, NULL, statistics_manager, NULL);
}

static void ball_received_signal_handler(int signal_number) {
    if (signal_number == SIGRTMAX) {
        balls_action();
    } else if (signal_number == SIGRTMAX - 1) {
        finished = true;
        if (child_process != 0) kill(child_process, SIGRTMAX - 1);
        waitpid(child_process, NULL, 0);
    }
}

void *statistics_manager() {
    struct sigaction signal_action;
    signal_action.sa_handler = statistics_signal_handler;
    signal_action.sa_flags = 0;
    sigaction(SIGRTMIN, &signal_action, NULL);

    while (!finished) {
        pause();
    }

    print("Statistic process exited!");
}

static void statistics_signal_handler(int signal_number) {
    if (signal_number == SIGRTMIN) {
        write_statistics_via_named_pipe();
    }
}

void print(const char *format, ...) {
    va_list argptr;
    va_start(argptr, format);
    printf("%s%d%s", "[", process_no, "] ");
    vprintf(format, argptr);
    printf("%s", "\n");
    va_end(argptr);
}

void print_stats(const int *data) {
    print("%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d", data[0], " ", data[1], " ", data[2], " | ", data[3], " ", data[4], " ",
          data[5], " | ", data[6], " ", data[7], " ", data[8]);
}

void semopen(struct sembuf semaphore_struct) {
    semaphore_struct.sem_num = 0;
    semaphore_struct.sem_op = -1;
    semaphore_struct.sem_flg = 0;
    semop(sem, &semaphore_struct, 1);
}

void semclose(struct sembuf semaphore_struct) {
    semaphore_struct.sem_num = 0;
    semaphore_struct.sem_op = 1;
    semaphore_struct.sem_flg = 0;
    semop(sem, &semaphore_struct, 1);
}