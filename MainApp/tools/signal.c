#include <stdlib.h>
#include <signal.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    int pid = atoi(argv[1]);
    printf("%s%d%s", "PID: ", pid, "\n");
    kill(pid, SIGUSR1);
}
