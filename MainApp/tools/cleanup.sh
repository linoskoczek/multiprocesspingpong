#!/bin/bash

echo -en "Cleaning up shared memory:\t"
TEMP=$(ipcs -m | grep 654321 | cut -d" " -f2)
if [[ -n "$TEMP" ]]; then
    ipcrm shm ${TEMP}
else
    echo "No cleaning required"
fi

echo -en "Cleaning up semaphore:\t\t"
TEMP=$(ipcs -s | grep 123456 | cut -d" " -f2)
if [[ -n "$TEMP" ]]; then
    ipcrm sem ${TEMP}
else
    echo "No cleaning required"
fi

echo -en "Cleaning up named pipe:\t\t"
if [[ -f /tmp/ppstats ]]; then
    unlink /tmp/ppstats
    echo "Cleaned"
else
    echo "No cleaning required"
fi

killall MainApp;